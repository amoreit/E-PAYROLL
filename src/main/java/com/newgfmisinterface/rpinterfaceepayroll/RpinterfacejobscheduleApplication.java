package com.newgfmisinterface.rpinterfaceepayroll;

import com.newgfmisinterface.rpinterfaceepayroll.properties.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class RpinterfacejobscheduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RpinterfacejobscheduleApplication.class, args);
	}

}
