package com.newgfmisinterface.rpinterfaceepayroll.dto;

public class EpayrollLogDto {
    private String trnEpayrollLogId;
    private String fileName;
    private String transDate;
    private String docType;
    private String status;
    private String totAmt;
    private String totRec;
    private String postRec;
    private String errRec;
    private String uploadName;
    private String uploadStart;
    private String uploadFinish;
    private String postStart;
    private String postFinish;
    private String postName;
    private String createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private String updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private String isactive;

    public EpayrollLogDto() {
    }

    public EpayrollLogDto(String trnEpayrollLogId, String fileName, String transDate, String docType, String status, String totAmt, String totRec, String postRec, String errRec, String uploadName, String uploadStart, String uploadFinish, String postStart, String postFinish, String postName, String createdDate, String createdPage, String createdUser, String ipaddr, String updatedDate, String updatedPage, String updatedUser, String recordStatus, String isactive) {
        this.trnEpayrollLogId = trnEpayrollLogId;
        this.fileName = fileName;
        this.transDate = transDate;
        this.docType = docType;
        this.status = status;
        this.totAmt = totAmt;
        this.totRec = totRec;
        this.postRec = postRec;
        this.errRec = errRec;
        this.uploadName = uploadName;
        this.uploadStart = uploadStart;
        this.uploadFinish = uploadFinish;
        this.postStart = postStart;
        this.postFinish = postFinish;
        this.postName = postName;
        this.createdDate = createdDate;
        this.createdPage = createdPage;
        this.createdUser = createdUser;
        this.ipaddr = ipaddr;
        this.updatedDate = updatedDate;
        this.updatedPage = updatedPage;
        this.updatedUser = updatedUser;
        this.recordStatus = recordStatus;
        this.isactive = isactive;
    }

    public String getTrnEpayrollLogId() {
        return trnEpayrollLogId;
    }

    public void setTrnEpayrollLogId(String trnEpayrollLogId) {
        this.trnEpayrollLogId = trnEpayrollLogId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotAmt() {
        return totAmt;
    }

    public void setTotAmt(String totAmt) {
        this.totAmt = totAmt;
    }

    public String getTotRec() {
        return totRec;
    }

    public void setTotRec(String totRec) {
        this.totRec = totRec;
    }

    public String getPostRec() {
        return postRec;
    }

    public void setPostRec(String postRec) {
        this.postRec = postRec;
    }

    public String getErrRec() {
        return errRec;
    }

    public void setErrRec(String errRec) {
        this.errRec = errRec;
    }

    public String getUploadName() {
        return uploadName;
    }

    public void setUploadName(String uploadName) {
        this.uploadName = uploadName;
    }

    public String getUploadStart() {
        return uploadStart;
    }

    public void setUploadStart(String uploadStart) {
        this.uploadStart = uploadStart;
    }

    public String getUploadFinish() {
        return uploadFinish;
    }

    public void setUploadFinish(String uploadFinish) {
        this.uploadFinish = uploadFinish;
    }

    public String getPostStart() {
        return postStart;
    }

    public void setPostStart(String postStart) {
        this.postStart = postStart;
    }

    public String getPostFinish() {
        return postFinish;
    }

    public void setPostFinish(String postFinish) {
        this.postFinish = postFinish;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    @Override
    public String toString() {
        return "EpayrollLogDto{" +
                "trnEpayrollLogId='" + trnEpayrollLogId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", transDate='" + transDate + '\'' +
                ", docType='" + docType + '\'' +
                ", status='" + status + '\'' +
                ", totAmt='" + totAmt + '\'' +
                ", totRec='" + totRec + '\'' +
                ", postRec='" + postRec + '\'' +
                ", errRec='" + errRec + '\'' +
                ", uploadName='" + uploadName + '\'' +
                ", uploadStart='" + uploadStart + '\'' +
                ", uploadFinish='" + uploadFinish + '\'' +
                ", postStart='" + postStart + '\'' +
                ", postFinish='" + postFinish + '\'' +
                ", postName='" + postName + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", createdPage='" + createdPage + '\'' +
                ", createdUser='" + createdUser + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", updatedPage='" + updatedPage + '\'' +
                ", updatedUser='" + updatedUser + '\'' +
                ", recordStatus='" + recordStatus + '\'' +
                ", isactive='" + isactive + '\'' +
                '}';
    }
}
