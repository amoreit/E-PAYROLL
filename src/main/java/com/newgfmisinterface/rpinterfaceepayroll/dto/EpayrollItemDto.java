package com.newgfmisinterface.rpinterfaceepayroll.dto;

public class EpayrollItemDto {
    private String trnEpayrollId;
    private String fileName;
    private String lineType;
    private String docType;
    private String agencyCode;
    private String docDate;
    private String postedDate;
    private String refNo;
    private String currency;
    private String revDate;
    private String revReason;
    private String paymentCenter;
    private String itemno;
    private String postingKey;
    private String accType;
    private String accCode;
    private String businessArea;
    private String cct;
    private String fundSource;
    private String fundCenter;
    private String functionalArea;
    private String businessProcess;
    private String amount;
    private String depRef;
    private String assignment;
    private String earmarkedFundNo;
    private String earmarkedFundItem;
    private String bank;
    private String gpsc;
    private String subAcc;
    private String subAccOwner;
    private String paymentCenterDetail;
    private String depOwner;
    private String depAcc;
    private String desc1;
    private String paymentTerm;
    private String paymentMethod;
    private String witType;
    private String wthCode;
    private String wthBaseAmt;
    private String wthAmt;
    private String searchTermVendor;
    private String vendorDepAcc;
    private String vendorBank;
    private String vendorAccName;
    private String companyId;
    private String tradingBa;
    private String docNo;
    private String uname;
    private String createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private String updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private String isactive;

    public EpayrollItemDto() {
    }

    public EpayrollItemDto(String trnEpayrollId, String fileName, String lineType, String docType, String agencyCode, String docDate, String postedDate, String refNo, String currency, String revDate, String revReason, String paymentCenter, String itemno, String postingKey, String accType, String accCode, String businessArea, String cct, String fundSource, String fundCenter, String functionalArea, String businessProcess, String amount, String depRef, String assignment, String earmarkedFundNo, String earmarkedFundItem, String bank, String gpsc, String subAcc, String subAccOwner, String paymentCenterDetail, String depOwner, String depAcc, String desc1, String paymentTerm, String paymentMethod, String witType, String wthCode, String wthBaseAmt, String wthAmt, String searchTermVendor, String vendorDepAcc, String vendorBank, String vendorAccName, String companyId, String tradingBa, String docNo, String uname, String createdDate, String createdPage, String createdUser, String ipaddr, String updatedDate, String updatedPage, String updatedUser, String recordStatus, String isactive) {
        this.trnEpayrollId = trnEpayrollId;
        this.fileName = fileName;
        this.lineType = lineType;
        this.docType = docType;
        this.agencyCode = agencyCode;
        this.docDate = docDate;
        this.postedDate = postedDate;
        this.refNo = refNo;
        this.currency = currency;
        this.revDate = revDate;
        this.revReason = revReason;
        this.paymentCenter = paymentCenter;
        this.itemno = itemno;
        this.postingKey = postingKey;
        this.accType = accType;
        this.accCode = accCode;
        this.businessArea = businessArea;
        this.cct = cct;
        this.fundSource = fundSource;
        this.fundCenter = fundCenter;
        this.functionalArea = functionalArea;
        this.businessProcess = businessProcess;
        this.amount = amount;
        this.depRef = depRef;
        this.assignment = assignment;
        this.earmarkedFundNo = earmarkedFundNo;
        this.earmarkedFundItem = earmarkedFundItem;
        this.bank = bank;
        this.gpsc = gpsc;
        this.subAcc = subAcc;
        this.subAccOwner = subAccOwner;
        this.paymentCenterDetail = paymentCenterDetail;
        this.depOwner = depOwner;
        this.depAcc = depAcc;
        this.desc1 = desc1;
        this.paymentTerm = paymentTerm;
        this.paymentMethod = paymentMethod;
        this.witType = witType;
        this.wthCode = wthCode;
        this.wthBaseAmt = wthBaseAmt;
        this.wthAmt = wthAmt;
        this.searchTermVendor = searchTermVendor;
        this.vendorDepAcc = vendorDepAcc;
        this.vendorBank = vendorBank;
        this.vendorAccName = vendorAccName;
        this.companyId = companyId;
        this.tradingBa = tradingBa;
        this.docNo = docNo;
        this.uname = uname;
        this.createdDate = createdDate;
        this.createdPage = createdPage;
        this.createdUser = createdUser;
        this.ipaddr = ipaddr;
        this.updatedDate = updatedDate;
        this.updatedPage = updatedPage;
        this.updatedUser = updatedUser;
        this.recordStatus = recordStatus;
        this.isactive = isactive;
    }

    public String getTrnEpayrollId() {
        return trnEpayrollId;
    }

    public void setTrnEpayrollId(String trnEpayrollId) {
        this.trnEpayrollId = trnEpayrollId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRevDate() {
        return revDate;
    }

    public void setRevDate(String revDate) {
        this.revDate = revDate;
    }

    public String getRevReason() {
        return revReason;
    }

    public void setRevReason(String revReason) {
        this.revReason = revReason;
    }

    public String getPaymentCenter() {
        return paymentCenter;
    }

    public void setPaymentCenter(String paymentCenter) {
        this.paymentCenter = paymentCenter;
    }

    public String getItemno() {
        return itemno;
    }

    public void setItemno(String itemno) {
        this.itemno = itemno;
    }

    public String getPostingKey() {
        return postingKey;
    }

    public void setPostingKey(String postingKey) {
        this.postingKey = postingKey;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccCode() {
        return accCode;
    }

    public void setAccCode(String accCode) {
        this.accCode = accCode;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public String getCct() {
        return cct;
    }

    public void setCct(String cct) {
        this.cct = cct;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getFundCenter() {
        return fundCenter;
    }

    public void setFundCenter(String fundCenter) {
        this.fundCenter = fundCenter;
    }

    public String getFunctionalArea() {
        return functionalArea;
    }

    public void setFunctionalArea(String functionalArea) {
        this.functionalArea = functionalArea;
    }

    public String getBusinessProcess() {
        return businessProcess;
    }

    public void setBusinessProcess(String businessProcess) {
        this.businessProcess = businessProcess;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDepRef() {
        return depRef;
    }

    public void setDepRef(String depRef) {
        this.depRef = depRef;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getEarmarkedFundNo() {
        return earmarkedFundNo;
    }

    public void setEarmarkedFundNo(String earmarkedFundNo) {
        this.earmarkedFundNo = earmarkedFundNo;
    }

    public String getEarmarkedFundItem() {
        return earmarkedFundItem;
    }

    public void setEarmarkedFundItem(String earmarkedFundItem) {
        this.earmarkedFundItem = earmarkedFundItem;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getGpsc() {
        return gpsc;
    }

    public void setGpsc(String gpsc) {
        this.gpsc = gpsc;
    }

    public String getSubAcc() {
        return subAcc;
    }

    public void setSubAcc(String subAcc) {
        this.subAcc = subAcc;
    }

    public String getSubAccOwner() {
        return subAccOwner;
    }

    public void setSubAccOwner(String subAccOwner) {
        this.subAccOwner = subAccOwner;
    }

    public String getPaymentCenterDetail() {
        return paymentCenterDetail;
    }

    public void setPaymentCenterDetail(String paymentCenterDetail) {
        this.paymentCenterDetail = paymentCenterDetail;
    }

    public String getDepOwner() {
        return depOwner;
    }

    public void setDepOwner(String depOwner) {
        this.depOwner = depOwner;
    }

    public String getDepAcc() {
        return depAcc;
    }

    public void setDepAcc(String depAcc) {
        this.depAcc = depAcc;
    }

    public String getDesc1() {
        return desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getWitType() {
        return witType;
    }

    public void setWitType(String witType) {
        this.witType = witType;
    }

    public String getWthCode() {
        return wthCode;
    }

    public void setWthCode(String wthCode) {
        this.wthCode = wthCode;
    }

    public String getWthBaseAmt() {
        return wthBaseAmt;
    }

    public void setWthBaseAmt(String wthBaseAmt) {
        this.wthBaseAmt = wthBaseAmt;
    }

    public String getWthAmt() {
        return wthAmt;
    }

    public void setWthAmt(String wthAmt) {
        this.wthAmt = wthAmt;
    }

    public String getSearchTermVendor() {
        return searchTermVendor;
    }

    public void setSearchTermVendor(String searchTermVendor) {
        this.searchTermVendor = searchTermVendor;
    }

    public String getVendorDepAcc() {
        return vendorDepAcc;
    }

    public void setVendorDepAcc(String vendorDepAcc) {
        this.vendorDepAcc = vendorDepAcc;
    }

    public String getVendorBank() {
        return vendorBank;
    }

    public void setVendorBank(String vendorBank) {
        this.vendorBank = vendorBank;
    }

    public String getVendorAccName() {
        return vendorAccName;
    }

    public void setVendorAccName(String vendorAccName) {
        this.vendorAccName = vendorAccName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getTradingBa() {
        return tradingBa;
    }

    public void setTradingBa(String tradingBa) {
        this.tradingBa = tradingBa;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    @Override
    public String toString() {
        return "EpayrollItemDto{" +
                "trnEpayrollId='" + trnEpayrollId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", lineType='" + lineType + '\'' +
                ", docType='" + docType + '\'' +
                ", agencyCode='" + agencyCode + '\'' +
                ", docDate='" + docDate + '\'' +
                ", postedDate='" + postedDate + '\'' +
                ", refNo='" + refNo + '\'' +
                ", currency='" + currency + '\'' +
                ", revDate='" + revDate + '\'' +
                ", revReason='" + revReason + '\'' +
                ", paymentCenter='" + paymentCenter + '\'' +
                ", itemno='" + itemno + '\'' +
                ", postingKey='" + postingKey + '\'' +
                ", accType='" + accType + '\'' +
                ", accCode='" + accCode + '\'' +
                ", businessArea='" + businessArea + '\'' +
                ", cct='" + cct + '\'' +
                ", fundSource='" + fundSource + '\'' +
                ", fundCenter='" + fundCenter + '\'' +
                ", functionalArea='" + functionalArea + '\'' +
                ", businessProcess='" + businessProcess + '\'' +
                ", amount='" + amount + '\'' +
                ", depRef='" + depRef + '\'' +
                ", assignment='" + assignment + '\'' +
                ", earmarkedFundNo='" + earmarkedFundNo + '\'' +
                ", earmarkedFundItem='" + earmarkedFundItem + '\'' +
                ", bank='" + bank + '\'' +
                ", gpsc='" + gpsc + '\'' +
                ", subAcc='" + subAcc + '\'' +
                ", subAccOwner='" + subAccOwner + '\'' +
                ", paymentCenterDetail='" + paymentCenterDetail + '\'' +
                ", depOwner='" + depOwner + '\'' +
                ", depAcc='" + depAcc + '\'' +
                ", desc1='" + desc1 + '\'' +
                ", paymentTerm='" + paymentTerm + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", witType='" + witType + '\'' +
                ", wthCode='" + wthCode + '\'' +
                ", wthBaseAmt='" + wthBaseAmt + '\'' +
                ", wthAmt='" + wthAmt + '\'' +
                ", searchTermVendor='" + searchTermVendor + '\'' +
                ", vendorDepAcc='" + vendorDepAcc + '\'' +
                ", vendorBank='" + vendorBank + '\'' +
                ", vendorAccName='" + vendorAccName + '\'' +
                ", companyId='" + companyId + '\'' +
                ", tradingBa='" + tradingBa + '\'' +
                ", docNo='" + docNo + '\'' +
                ", uname='" + uname + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", createdPage='" + createdPage + '\'' +
                ", createdUser='" + createdUser + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", updatedPage='" + updatedPage + '\'' +
                ", updatedUser='" + updatedUser + '\'' +
                ", recordStatus='" + recordStatus + '\'' +
                ", isactive='" + isactive + '\'' +
                '}';
    }
}
