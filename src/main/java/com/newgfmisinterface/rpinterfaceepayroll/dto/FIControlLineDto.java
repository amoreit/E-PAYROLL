package com.newgfmisinterface.rpinterfaceepayroll.dto;

public class FIControlLineDto {
    private String lineType;
    private String totalLineCount;
    private String zipFilename;

    public FIControlLineDto() {
    }

    public FIControlLineDto(String lineType, String totalLineCount, String zipFilename) {
        this.lineType = lineType;
        this.totalLineCount = totalLineCount;
        this.zipFilename = zipFilename;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getTotalLineCount() {
        return totalLineCount;
    }

    public void setTotalLineCount(String totalLineCount) {
        this.totalLineCount = totalLineCount;
    }

    public String getZipFilename() {
        return zipFilename;
    }

    public void setZipFilename(String zipFilename) {
        this.zipFilename = zipFilename;
    }

    @Override
    public String toString() {
        return "FIControlLineDto{" +
                "lineType='" + lineType + '\'' +
                ", totalLineCount='" + totalLineCount + '\'' +
                ", zipFilename='" + zipFilename + '\'' +
                '}';
    }
}
