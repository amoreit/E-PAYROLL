package com.newgfmisinterface.rpinterfaceepayroll.dto;

public class EpayrollErrorDto {
    private String trnEpayrollId;
    private String fileName;
    private String transDate;
    private String docType;
    private String itemno;
    private String msgno;
    private String msgid;
    private String message;
    private String uploadName;
    private String uploadDate;
    private String createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private String updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private String isactive;

    public EpayrollErrorDto() {
    }

    public EpayrollErrorDto(String trnEpayrollId, String fileName, String transDate, String docType, String itemno, String msgno, String msgid, String message, String uploadName, String uploadDate, String createdDate, String createdPage, String createdUser, String ipaddr, String updatedDate, String updatedPage, String updatedUser, String recordStatus, String isactive) {
        this.trnEpayrollId = trnEpayrollId;
        this.fileName = fileName;
        this.transDate = transDate;
        this.docType = docType;
        this.itemno = itemno;
        this.msgno = msgno;
        this.msgid = msgid;
        this.message = message;
        this.uploadName = uploadName;
        this.uploadDate = uploadDate;
        this.createdDate = createdDate;
        this.createdPage = createdPage;
        this.createdUser = createdUser;
        this.ipaddr = ipaddr;
        this.updatedDate = updatedDate;
        this.updatedPage = updatedPage;
        this.updatedUser = updatedUser;
        this.recordStatus = recordStatus;
        this.isactive = isactive;
    }

    public String getTrnEpayrollId() {
        return trnEpayrollId;
    }

    public void setTrnEpayrollId(String trnEpayrollId) {
        this.trnEpayrollId = trnEpayrollId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getItemno() {
        return itemno;
    }

    public void setItemno(String itemno) {
        this.itemno = itemno;
    }

    public String getMsgno() {
        return msgno;
    }

    public void setMsgno(String msgno) {
        this.msgno = msgno;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUploadName() {
        return uploadName;
    }

    public void setUploadName(String uploadName) {
        this.uploadName = uploadName;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    @Override
    public String toString() {
        return "EpayrollErrorDto{" +
                "trnEpayrollId='" + trnEpayrollId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", transDate='" + transDate + '\'' +
                ", docType='" + docType + '\'' +
                ", itemno='" + itemno + '\'' +
                ", msgno='" + msgno + '\'' +
                ", msgid='" + msgid + '\'' +
                ", message='" + message + '\'' +
                ", uploadName='" + uploadName + '\'' +
                ", uploadDate='" + uploadDate + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", createdPage='" + createdPage + '\'' +
                ", createdUser='" + createdUser + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", updatedPage='" + updatedPage + '\'' +
                ", updatedUser='" + updatedUser + '\'' +
                ", recordStatus='" + recordStatus + '\'' +
                ", isactive='" + isactive + '\'' +
                '}';
    }
}
