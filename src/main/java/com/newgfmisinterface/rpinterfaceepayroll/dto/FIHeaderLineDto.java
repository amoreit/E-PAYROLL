package com.newgfmisinterface.rpinterfaceepayroll.dto;

public class FIHeaderLineDto {
    private String lineType;
    private String documentType;
    private String companyCode;
    private String documentDate;
    private String postingDate;
    private String reference;
    private String currency;
    private String reverseDate;
    private String reversalReason;
    private String PaymentCenterHeader;
    private String excelFilename;
    private String ownerUser;

    public FIHeaderLineDto() {
    }

    public FIHeaderLineDto(String lineType, String documentType, String companyCode, String documentDate, String postingDate, String reference, String currency, String reverseDate, String reversalReason, String PaymentCenterHeader, String excelFilename, String ownerUser) {
        this.lineType = lineType;
        this.documentType = documentType;
        this.companyCode = companyCode;
        this.documentDate = documentDate;
        this.postingDate = postingDate;
        this.reference = reference;
        this.currency = currency;
        this.reverseDate = reverseDate;
        this.reversalReason = reversalReason;
        this.PaymentCenterHeader = PaymentCenterHeader;
        this.excelFilename = excelFilename;
        this.ownerUser = ownerUser;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getReverseDate() {
        return reverseDate;
    }

    public void setReverseDate(String reverseDate) {
        this.reverseDate = reverseDate;
    }

    public String getReversalReason() {
        return reversalReason;
    }

    public void setReversalReason(String reversalReason) {
        this.reversalReason = reversalReason;
    }

    public String getPaymentCenterHeader() {
        return PaymentCenterHeader;
    }

    public void setPaymentCenterHeader(String paymentCenterHeader) {
        this.PaymentCenterHeader = paymentCenterHeader;
    }

    public String getExcelFilename() {
        return excelFilename;
    }

    public void setExcelFilename(String excelFilename) {
        this.excelFilename = excelFilename;
    }

    public String getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(String ownerUser) {
        this.ownerUser = ownerUser;
    }

    @Override
    public String toString() {
        return "FIHeaderLineDto{" +
                "lineType='" + lineType + '\'' +
                ", documentType='" + documentType + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", documentDate='" + documentDate + '\'' +
                ", postingDate='" + postingDate + '\'' +
                ", reference='" + reference + '\'' +
                ", currency='" + currency + '\'' +
                ", reverseDate='" + reverseDate + '\'' +
                ", reversalReason='" + reversalReason + '\'' +
                ", PaymentCenterHeader='" + PaymentCenterHeader + '\'' +
                ", excelFilename='" + excelFilename + '\'' +
                ", ownerUser='" + ownerUser + '\'' +
                '}';
    }
}
