//package com.newgfmisinterface.rpinterfaceepayroll.controller;
//
//import com.newgfmisinterface.rpinterfaceepayroll.dao.*;
//import com.newgfmisinterface.rpinterfaceepayroll.dto.ConvertAndPostingDto;
//import com.newgfmisinterface.rpinterfaceepayroll.dto.ResponseDto;
//import com.newgfmisinterface.rpinterfaceepayroll.job.ProcessJobCAP;
//import com.newgfmisinterface.rpinterfaceepayroll.job.ProcessJobSTM;
//import com.newgfmisinterface.rpinterfaceepayroll.job.ProcessJobAMQ;
//import com.newgfmisinterface.rpinterfaceepayroll.service.FileStorageService;
//import com.newgfmisinterface.rpinterfaceepayroll.service.ScanFilePathService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.TaskScheduler;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//public class ConvertFileController {
//    @Autowired
//    private FileStorageService fileStorageService;
//
//    @Autowired
//    private ScanFilePathService scanFilePath;
//
//    @Autowired
//    private TaskScheduler taskScheduler;
//
//    @Autowired
//    private ProcessJobCAP processJobCAP;
//
//    @Autowired
//    private ProcessJobSTM processJobSTM;
//
//    @Autowired
//    private ProcessJobAMQ processJobAMQ;
//
//    @Autowired
//    private EbiddingLogDao ebiddingLogDao;
//
//    @Autowired
//    private EbiddingItemDao ebiddingItemDao;
//
//    @Autowired
//    private EbiddingStatementDao ebiddingStatementDao;
//
//    @Autowired
//    private EbiddingErrorDao ebiddingErrorDao;
//
//    @Autowired
//    private EbiddingActiveMQDao ebiddingActiveMQDao;
//
//    @Autowired
//    private EbiddingSendFileDao ebiddingSendFileDao;
//
//    @GetMapping("/convertFileEbidding")
//    public ResponseDto convertFileEbidding(ConvertAndPostingDto convertAndPostingDto) {
//        ArrayList<List<Map<String, Object>>> resultList = null;
//        String projectFile = convertAndPostingDto.getFileName();
//        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);
//
//        String moveFileName = moveFile.toString();
//        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
//        if (moveFileName.equals(null) || moveFileName.isEmpty()) {
//            return new ResponseDto(2, "fail", null);
//        } else {
//            boolean statusMove = fileStorageService.moveFileToConvert(moveFileName);
//            if (statusMove) {
//                List<String> fileList = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectFile);
//                String transDate = convertAndPostingDto.getTransDate();
////                String fileDate = convertAndPostingDto.getFileDate();
////                String docTyp = convertAndPostingDto.getDocType();
//                String fileName = fileList.toString();
//                fileName = fileName.replaceAll("[\\[\\]]", "");
//                try {
//                    resultList = processJobCAP.jobRunnerConvertFile(fileName, transDate);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        try {
//            if(resultList.size() > 0){
//                return new ResponseDto(1, "success", resultList);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseDto(0, "fail", null);
//        }
//
//
//    }
//    @GetMapping("/postFileEbidding")
//    public ResponseDto postFileEbidding(ConvertAndPostingDto convertAndPostingDto) {
//        int statusInsert = 0;
//        String projectFile = convertAndPostingDto.getFileName();
//            List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);
//
//            String moveFileName = moveFile.toString();
//            moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
//            if (moveFileName.equals(null) || moveFileName.isEmpty()) {
//                return new ResponseDto(2, "fail", null);
//            } else {
//                boolean statusMove = fileStorageService.moveFileToConvert(moveFileName);
//                if (statusMove) {
//                    List<String> fileList = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectFile);
//                    String fileName = fileList.toString();
//                    fileName = fileName.replaceAll("[\\[\\]]", "");
//                    int countCheckStatusInsert = ebiddingLogDao.countCheckStatusInsert(fileName);
//                    statusInsert = countCheckStatusInsert > 0 ? 4 : statusInsert;
//                    if(countCheckStatusInsert == 0) {
//                        try {
//                            statusInsert = processJobCAP.jobRunnerPostFile(fileName);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
////        }
//        try {
//            if(statusInsert == 1){
//                return new ResponseDto(1, "success", null);
//            }else if(statusInsert == 3){
//                return new ResponseDto(3, "fail", null);
//            }else if(statusInsert == 4){
//                return new ResponseDto(4, "fail", null);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseDto(0, "fail", null);
//        }
//
//
//    }
//    @GetMapping("/runTestFileStatement")
//    public ResponseDto runTestFileStatement(ConvertAndPostingDto convertAndPostingDto) {
//        String projectFile = convertAndPostingDto.getFileName();
//        String transDate = convertAndPostingDto.getTransDate();
//        String fileDate = convertAndPostingDto.getFileDate();
//        List<Map<String, Object>> resultList = null;
//        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);
//
//        String moveFileName = moveFile.toString();
//        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
//        int countFile = moveFileName == ""?0:moveFile.size();
////        int countInsert = ebiddingStatementDao.countCheckTestAndProcess(projectFile);
////        if(countInsert > 0){
////            try {
////                ebiddingStatementDao.updateProcess(projectFile);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////        }else {
//            resultList = processJobCAP.jobRunnerTestStatement(projectFile,transDate,fileDate,countFile);
////        }
//
//        try {
//            if(resultList.get(6).get("statusFile").equals("PASS")){
//                return new ResponseDto(1, "success", resultList);
//            }else {
//                return new ResponseDto(2, "error", resultList);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseDto(0, "fail", null);
//        }
//    }
//    @GetMapping("/runProcessFileStatementEbidding")
//    public ResponseDto runProcessFileStatement(ConvertAndPostingDto convertAndPostingDto) {
//        String projectFile = convertAndPostingDto.getFileName();
//        String transDate = convertAndPostingDto.getTransDate();
//        String fileDate = convertAndPostingDto.getFileDate();
//        List<Map<String, Object>> resultList = null;
//        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);
//        int countStatusInsert = 0;
//        String moveFileName = moveFile.toString();
//        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
//        int countFile = moveFileName == ""?0:moveFile.size();
//        int countInsert = ebiddingStatementDao.countCheckTestAndProcess(projectFile);
//        if(countInsert > 0){
//            try {
//                resultList = ebiddingStatementDao.updateProcess(projectFile);
//                countStatusInsert = 1;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }else {
//        resultList = processJobCAP.jobRunnerProcessStatement(projectFile,transDate,fileDate,countFile);
//            countStatusInsert = 2;
//        }
//
//        try {
//            if(countStatusInsert <= 1){
//                return new ResponseDto(1, "success", resultList);
//            }else if(countStatusInsert >= 2){
//                return new ResponseDto(2, "error", resultList);
//            }else {
//                return new ResponseDto(0, "error", resultList);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseDto(0, "fail", null);
//        }
////        String projectFile = convertAndPostingDto.getFileName();
////        String transDate = convertAndPostingDto.getTransDate();
////        int StatusInsert = 0;
////        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);
////
////        String moveFileName = moveFile.toString();
////        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
////        if (moveFileName.equals(null) || moveFileName.isEmpty()) {
////            return new ResponseDto(2, "fail", null);
////        } else {
////            boolean statusMove = fileStorageService.moveFileToConvert(moveFileName);
////            if(statusMove){
////                StatusInsert = processJobCAP.jobRunnerPostStatement(projectFile,transDate);
////            }
////        }
////        try {
////            if(StatusInsert > 0){
////                return new ResponseDto(1, "success", null);
////            }else {
////                return new ResponseDto(0, "fail", null);
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////            return new ResponseDto(0, "fail", null);
////        }
//    }
//    @GetMapping("/postingFileEbiddingActiveMq")
//    public ResponseDto postingFileEbiddingActiveMq(ConvertAndPostingDto convertAndPostingDto) {
//        ArrayList<List<Map<String, Object>>> resultList = null;
//        String fileName = convertAndPostingDto.getFileName();
//        String transDate = convertAndPostingDto.getTransDate();
//        String docTyp = convertAndPostingDto.getDocType();
//
//        try {
//            resultList = ebiddingActiveMQDao.jobRunnerPostingFileActiveMq(fileName,transDate,docTyp);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            if(resultList.size() > 0){
//                return new ResponseDto(1, "success", resultList);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseDto(0, "fail", null);
//        }
//
//
//    }
//
//}
