package com.newgfmisinterface.rpinterfaceepayroll.dao;

//import com.newgfmisinterface.rpinterfaceepayroll.dto.EbiddingLogDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class EpayrollSendPathDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EpayrollSendPathDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Map<String, Object>> getEpayrollPathUploadFile() throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql ="SELECT VALUE AS uploadpath FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='INCOMING_PATH_EPAYROLL_BATCHJOB'";
        System.out.println("<<< sql: " + sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
    public List<Map<String, Object>> getEpayrollPathOutputFile() throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql ="SELECT VALUE AS outputpath FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='OUTGOING_PATH_EPAYROLL_BATCHJOB'";
        System.out.println("<<< sql: " + sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
}
