package com.newgfmisinterface.rpinterfaceepayroll.validate;

public class FileNameFormatValidate {
    public boolean isFileProjectCodeAll(String fileName , String projectName) {

        String expFileName = fileName.toUpperCase();
        String expProjectName = projectName.toUpperCase();
        String subsTringFileName = expFileName.substring(0, 9);

        if (!subsTringFileName.equals(expProjectName)) {
            return false;
        }
        return true;
    }
    public boolean isFileProjectCode(String fileName , String projectName) {

        String expFileName = fileName.toLowerCase();
        String expProjectName = projectName.toLowerCase();
//        String subsTringFileName = expFileName.substring(0, 13);

        if (!expFileName.equals(expProjectName)) {
            return false;
        }
        return true;
    }
    public boolean isConvertAndPostFileProjectCode(String fileName , String projectName) {

        String expFileName = fileName.toLowerCase();
        String expProjectName = projectName.toLowerCase();

        if (!expFileName.equals(expProjectName)) {
            return false;
        }
        return true;
    }
    public boolean isFileNameOutput(String fileName , String projectName) {

        String expFileName = fileName.toUpperCase();
        String expProjectName = projectName.toUpperCase();
        String subStringProjectName = expProjectName.substring(0, 21);
        String subsTringFileName = expFileName.substring(0, 21);

        if (!subsTringFileName.equals(subStringProjectName)) {
            return false;
        }
        return true;
    }
    public boolean isFileProject(String fileName , String projectName) {

        String expFileName = fileName.toUpperCase();
        String expProjectName = projectName.toUpperCase();
        String subsTringFileName = expFileName.substring(0, 12);

        if (!subsTringFileName.equals(expProjectName)) {
            return false;
        }
        return true;
    }
}
