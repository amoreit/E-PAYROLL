package com.newgfmisinterface.rpinterfaceepayroll.job;

import com.newgfmisinterface.rpinterfaceepayroll.dao.EpayrollItemDao;
import com.newgfmisinterface.rpinterfaceepayroll.dto.FIControlLineDto;
import com.newgfmisinterface.rpinterfaceepayroll.dto.FIHeaderLineDto;
import com.newgfmisinterface.rpinterfaceepayroll.dto.FIItemLineDto;
import com.newgfmisinterface.rpinterfaceepayroll.dto.EpayrollItemDto;
import com.newgfmisinterface.rpinterfaceepayroll.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceepayroll.service.ScanFilePathService;
import com.newgfmisinterface.rpinterfaceepayroll.validate.TextPayrollValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class ProcessJobFI {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobFI.class);
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private EpayrollItemDao epayrollItemDao;

    private TextPayrollValidate textValidate = new TextPayrollValidate();

    public void jobRunner(String fileName) {
        String dateCreate = new SimpleDateFormat("dd-MMM-yy").format(new Date());
        File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + fileName);
        String filenameFullPath = file.getAbsolutePath();
        List<String> fileByline = fileStorageService.readFilebyLine(filenameFullPath);
        FIHeaderLineDto lineHeader = textValidate.substringHeaderLineObj(fileByline.get(fileByline.size() - fileByline.size()));
        FIControlLineDto lineControl = textValidate.substringControlLineObj(fileByline.get(fileByline.size() - 1));
System.out.println("lineHeader"+lineHeader);
System.out.println("lineControl"+lineControl);
        int row = 0;
        int itemNo = 1;
        for (String line : fileByline) {
            row = row + 1;
            if (row == 1 || row == fileByline.size()) {
                continue;
            }
System.out.println("line"+line);
            FIItemLineDto lineObj = textValidate.substringItemLineObj(line);
            EpayrollItemDto saveItem = new EpayrollItemDto();
            saveItem.setFileName(fileName);
            saveItem.setLineType(lineObj.getLineType());
            saveItem.setDocType(lineHeader.getDocumentType());
            saveItem.setAgencyCode(lineHeader.getCompanyCode());
            saveItem.setDocDate(lineHeader.getDocumentDate());
            saveItem.setPostedDate(lineHeader.getPostingDate());
            saveItem.setRefNo(lineHeader.getReference());
            saveItem.setCurrency(lineHeader.getCurrency());
            saveItem.setRevDate(lineHeader.getReverseDate());
            saveItem.setRevReason(lineHeader.getReversalReason());
            saveItem.setPaymentCenter(lineHeader.getPaymentCenterHeader());
            saveItem.setItemno(""+itemNo);
            saveItem.setPostingKey(lineObj.getPostingKey());
            saveItem.setAccType(lineObj.getAccountType());
            saveItem.setAccCode(lineObj.getAccountCode());
            saveItem.setBusinessArea(lineObj.getBusinessArea());
            saveItem.setCct(lineObj.getCostCenter());
            saveItem.setFundSource(lineObj.getFund());
            saveItem.setFundCenter(lineObj.getFundCenter());
            saveItem.setFunctionalArea(lineObj.getFunctionalArea());
            saveItem.setBusinessProcess(lineObj.getBusinessProcess());
            saveItem.setAmount(lineObj.getAmount());
            saveItem.setDepRef(lineObj.getDepositReference());
            saveItem.setAssignment(lineObj.getAssignment());
            saveItem.setEarmarkedFundNo(lineObj.getEarmarkedFundNo());
            saveItem.setEarmarkedFundItem(lineObj.getEarmarkedFundItem());
            saveItem.setBank(lineObj.getBank());
            saveItem.setGpsc(lineObj.getGpsc());
            saveItem.setSubAcc(lineObj.getSubAccount());
            saveItem.setSubAccOwner(lineObj.getSubAccountOwner());
            saveItem.setPaymentCenterDetail(lineObj.getPaymentCenterDetail());
            saveItem.setDepOwner(lineObj.getDepositOwner());
            saveItem.setDepAcc(lineObj.getDeposit());
            saveItem.setDesc1(lineObj.getText());
            saveItem.setPaymentTerm(lineObj.getPaymentTerm());
            saveItem.setPaymentMethod(lineObj.getPaymentMethod());
            saveItem.setWitType(lineObj.getWTType());
            saveItem.setWthCode(lineObj.getWTCode());
            saveItem.setWthBaseAmt(lineObj.getWTBaseAmount());
            saveItem.setWthAmt(lineObj.getWTAmount());
            saveItem.setSearchTermVendor(lineObj.getSearchTermVendor());
            saveItem.setVendorDepAcc(lineObj.getVendorDepositAC());
            saveItem.setVendorBank(lineObj.getPartnerBank());
            saveItem.setVendorAccName(lineObj.getVendorAccName());
            saveItem.setCompanyId(lineObj.getCompanyIDTradingPartner());
            saveItem.setTradingBa(lineObj.getTradingBA());
            saveItem.setDocNo(null);
            saveItem.setUname(null);
            saveItem.setCreatedDate(dateCreate);
            saveItem.setCreatedPage("");
            saveItem.setCreatedUser("");
            saveItem.setIpaddr("");
            saveItem.setUpdatedDate(dateCreate);
            saveItem.setUpdatedPage("");
            saveItem.setUpdatedUser("");
            saveItem.setRecordStatus("A");
            saveItem.setIsactive("Y");
System.out.println(saveItem.toString());
            try {
                epayrollItemDao.addItems(saveItem);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            itemNo = itemNo+1;
        }
        try {
            fileStorageService.MoveFiletoSuccessDirDate(fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
